<?php

return [
    'category'         => 'Categorie',
    'content'          => 'Content',
    'details'          => 'Details',
    'excerpt'          => 'Korte omschrijving',
    'image'            => 'Afbeelding',
    'meta_description' => 'Meta beschrijving',
    'meta_keywords'    => 'Meta sleutelwoorden',
    'new'              => 'Toevoegen',
    'seo_content'      => 'SEO inhoud',
    'seo_title'        => 'Seo titel',
    'slug'             => 'URL slug',
    'status'           => 'Status',
    'status_draft'     => 'Concept',
    'status_pending'   => 'in afwachting',
    'status_published' => 'gepubliceerd',
    'title'            => 'Titel',
    'title_sub'        => ' ',
    'update'           => 'Wijzigen ',
];
