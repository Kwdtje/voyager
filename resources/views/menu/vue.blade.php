<b-collapse is-nav id="nav_collapse">
    <b-navbar-nav>
        @php

            if (Voyager::translatable($items)) {
                $items = $items->load('translations');
            }

        @endphp

        @foreach ($items as $item)

            @php
                $active = '';
            $originalItem = $item;
            if (Voyager::translatable($item)) {
                $item = $item->translate($options->locale);
            }

            $listItemClass = null;
            $linkAttributes =  null;
            $styles = null;
            $icon = null;
            $caret = null;

            // Background Color or Color
            if (isset($options->color) && $options->color == true) {
                $styles = 'color:'.$item->color;
            }
            if (isset($options->background) && $options->background == true) {
                $styles = 'background-color:'.$item->color;
            }

            // With Children Attributes
            if(!$originalItem->children->isEmpty()) {
                $linkAttributes =  'class="dropdown-toggle" data-toggle="dropdown"';
                $caret = '<span class="caret"></span>';

                if(url($item->link()) == url()->current()){
                    $listItemClass = 'dropdown active';
                }else{
                    $listItemClass = 'dropdown';
                }
            }
            if(url($item->link()) == url()->current()){
                    $active = 'active';
             }

            // Set Icon
            if(isset($options->icon) && $options->icon == true){
                $icon = '<i class="' . $item->icon_class . '"></i>';
            }

            @endphp

            <b-nav-item href="{{ url($item->link()) }}" target="{{ $item->target }}" {{$active}}
                    {{$listItemClass}} >
                {!! $icon !!}
                <span>{{ $item->title }}</span>
                {!! $caret !!}
                {{--@if(!$originalItem->children->isEmpty())--}}
                {{--@include('voyager::menu.bootstrap', ['items' => $originalItem->children, 'options' => $options, 'innerLoop' => true])--}}
                {{--@endif--}}
            </b-nav-item>
        @endforeach
    </b-navbar-nav>
</b-collapse>
