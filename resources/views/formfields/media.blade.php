<div class="panel panel-bordered panel-info">
    <div class="panel-heading">
        <h3 class="panel-title">Media</h3>
    </div>
    <div id="filemanager">
        <div id="toolbar">
            <div class="btn-group">
                <button type="button" class="btn btn-primary" id="upload"><i class="voyager-upload"></i>
                    {{ __('voyager::generic.upload') }}
                </button>
                <button type="button" class="btn btn-default" id="delete"><i class="voyager-trash"></i>
                    {{ __('voyager::generic.delete') }}
                </button>
                <button v-show="selectedFileIs('image')" type="button" class="btn btn-default" id="crop"><i
                            class="voyager-crop"></i>
                    {{ __('voyager::media.crop') }}
                </button>
            </div>
        </div>
        <div id="uploadPreview" style="display:none;"></div>

        <div id="uploadProgress" class="progress active progress-striped">
            <div class="progress-bar progress-bar-success" style="width: 0"></div>
        </div>
        <div id="content">
            <div class="">

                <div id="left">

                    <ul id="files">
                        <li v-for="(file,index) in files.items">
                            <div class="file_link" :data-folder="file.name" :data-index="index">
                                <div class="link_icon">
                                    <template v-if="file.type.includes('image')">
                                        <div class="img_icon" :style="imgIcon(file.path)"></div>
                                    </template>
                                        <input type="hidden" name="images[]" :value="file.url"/>
                                </div>
                            </div>
                        </li>

                    </ul>

                    <div id="file_loader">
                        <?php $admin_loader_img = Voyager::setting('admin.loader', ''); ?>
                        @if($admin_loader_img == '')
                            <img src="{{ voyager_asset('images/logo-icon.png') }}" alt="Voyager Loader">
                        @else
                            <img src="{{ Voyager::image($admin_loader_img) }}" alt="Voyager Loader">
                        @endif
                        <p>{{ __('voyager::media.loading') }}</p>
                    </div>

                    <div id="no_files">
                    </div>

                </div>

            </div>

            <div class="nothingfound">
                <div class="nofiles"></div>
                <span>{{ __('voyager::media.no_files_here') }}</span>
            </div>
        </div>
        <!-- Image Modal -->
        <div class="modal fade" id="imagemodal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>
                    <div class="modal-body">
                        <img :src="selected_file.path" class="img img-responsive" style="margin: 0 auto;">
                    </div>

                    <div class="modal-footer text-left">
                        <small class="image-title">@{{ selected_file.name }}</small>
                    </div>

                </div>
            </div>
        </div>
        <!-- End Image Modal -->

        <!-- Crop Image Modal -->
        <div class="modal fade modal-warning" id="confirm_crop_modal">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::media.crop_image') }}
                        </h4>
                    </div>

                    <div class="modal-body">
                        <div class="crop-container">
                            <img v-if="selectedFileIs('image')" id="cropping-image" class="img img-responsive"
                                 :src="selected_file.path + '?' + selected_file.last_modified"/>
                        </div>
                        <div class="new-image-info">
                            {{ __('voyager::media.width') }} <span
                                    id="new-image-width"></span>, {{ __('voyager::media.height') }}<span
                                    id="new-image-height"></span>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default"
                                data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                        <button type="button" class="btn btn-warning" id="crop_btn"
                                data-confirm="{{ __('voyager::media.crop_override_confirm') }}">{{ __('voyager::media.crop') }}</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Crop Image Modal -->

        <!-- Delete File Modal -->
        <div class="modal fade modal-danger" id="confirm_delete_modal">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"
                                aria-hidden="true">&times;</button>
                        <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                    </div>

                    <div class="modal-body">
                        <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                        <h5 class="folder_warning"><i class="voyager-warning"></i> {{ __('voyager::media.delete_folder_question') }}</h5>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                        <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Delete File Modal -->
    </div>
    <input type="hidden" id="storage_path" value="{{ storage_path() }}">
    <input type="hidden" id="base_url" value="{{ route('voyager.dashboard') }}">
    <input type="hidden" id="base_directory" value="/pages">
    <input type="hidden" id="model_id" value="{{ $dataTypeContent->id}}" >
    <input type="hidden" id="model_class" value="{{ get_class($dataTypeContent)}}" >

</div>
@section('javascript')
    <script>
        MediaManager();
    </script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.4.1/cropper.min.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.4.1/cropper.js"></script>
        <script src="https://unpkg.com/sortablejs@1.4.2"></script>
@stop