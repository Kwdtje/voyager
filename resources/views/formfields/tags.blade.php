<!-- ### SEO CONTENT ### -->
<div class="panel panel-bordered panel-info">
    <div class="panel-heading">
        <h3 class="panel-title"><i class="icon wb-tags"></i> Tags
        </h3>
        <div class="panel-actions">
            <a class="panel-action voyager-angle-down" data-toggle="panel-collapse"
               aria-hidden="true"></a>
        </div>
    </div>
    <div class="panel-body">
        <select
                class="form-control select2-taggable"
                name="tags[]" multiple
                data-route="{{ route('voyager.bread.'.str_slug('tags')) }}"
                data-label="tags"
                data-error-message="{{__('voyager::bread.error_tagging')}}">
            @php
                $selected_values = isset($dataTypeContent->tags) ? $dataTypeContent->tags->pluck('id')->all() : [];
                $relationshipOptions = \TCG\Voyager\Models\Tag::all();@endphp
                <option value="">{{__('voyager::generic.none')}}</option>
            @foreach($relationshipOptions as $relationshipOption)
                <option value="{{ $relationshipOption->id }}" @if(in_array($relationshipOption->id, $selected_values)){{ 'selected="selected"' }}@endif>{{ $relationshipOption->name}}</option>
            @endforeach

        </select>
    </div>
</div>