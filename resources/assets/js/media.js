module.exports = function(){
	if(document.getElementById('filemanager')){
		var manager = new Vue({
			el: '#filemanager',
			data: {
		  		files: '',
		  		folders: [],
		  		selected_file: '',
		  		directories: [],
			},
			methods: {
				selectedFileIs: function(val){
				    if(this.selected_file) {
                        if (typeof(this.selected_file.type) != 'undefined' && this.selected_file.type.includes(val)) {
                            return true;
                        }
                    }
					return false;
				},
				imgIcon: function(path){
					return 'background-size: cover; background-image: url("' + path + '"); background-repeat:no-repeat; background-position:center center;display:inline-block;';
				},
				dateFilter: function(date){
					if(!date){
						return null;
					}
					var date = new Date(date * 1000);

					var month = "0" + (date.getMonth() + 1);
					var minutes = "0" + date.getMinutes();
					var seconds = "0" + date.getSeconds();

					var dateForamted = date.getFullYear() + '-' + month.substr(-2) + '-' + date.getDate() + ' ' + date.getHours() + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);

					return dateForamted;
				}
			}
		});


		CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

		var VoyagerMedia = function(o){
			var files = $('#files');
			var defaults = {
				baseUrl: "/admin",
                baseDirectory : '/'

            };
			var options = $.extend(true, defaults, o);
			this.init = function(){
				$("#upload").dropzone({
					url: options.baseUrl+"/media/upload",
					previewsContainer: "#uploadPreview",
					totaluploadprogress: function(uploadProgress, totalBytes, totalBytesSent){
						$('#uploadProgress .progress-bar').css('width', uploadProgress + '%');
						if(uploadProgress == 100){
							$('#uploadProgress').delay(1500).slideUp(function(){
								$('#uploadProgress .progress-bar').css('width', '0%');
							});

						}
					},
					processing: function(){
						$('#uploadProgress').fadeIn();
					},
					sending: function(file, xhr, formData) {
						formData.append("_token", CSRF_TOKEN);
						formData.append("upload_path", options.baseDirectory);
                        formData.append("id", options.id);
                        formData.append("model", options.model);
                    },
					success: function(e, res){
						if(res.success){
							toastr.success("Afbeelding succesvol opgeslagen!");
                            document.getElementById('model_id').value = res.id
                            options.id = res.id
                            var input = document.createElement("input");

                            input.setAttribute("type", "hidden");

                            input.setAttribute("name", "id");

                            input.setAttribute("value", res.id);

                            document.getElementById("form").appendChild(input);
						} else {
							toastr.error(res.message, "Afbeelding niet opgeslagen");
						}

					},
					error: function(e, res, xhr){
                        toastr.error(res.message, "Afbeelding niet opgeslagen");
					},
					queuecomplete: function(){
						getFiles(options.baseDirectory);
					}
				});

				getFiles(options.baseDirectory);


				files.on("dblclick", "li .file_link", function(){
					var type = manager.selected_file.type;

					var imageMimeTypes = ['image/jpeg', 'image/png', 'image/gif'];

					if (imageMimeTypes.indexOf(type) > -1) {
						$('#imagemodal').modal('show');
						return false;
					}

					if (type !== "folder") {
						return false;
					}

					manager.folders.push(manager.selected_file.name);
					getFiles(options.baseDirectory);
				});

				files.on("click", "li .file_link", function(e){
					var clicked = e.target;
					if(!$(clicked).hasClass('file_link')){
						clicked = $(e.target).closest('.file_link');
					}
					setCurrentSelected(clicked);
				});


				/********** TOOLBAR BUTTONS **********/

				$('#delete').click(function(){
					$('.confirm_delete_name').text(manager.selected_file.name);
					$('#confirm_delete_modal').modal('show');
				});

				$('#confirm_delete').click(function(){

					$.post(options.baseUrl+'/media/delete_file_folder', { folder_location: manager.folders, file_folder: manager.selected_file.name, type: manager.selected_file.type, _token: CSRF_TOKEN }, function(data){
						if(data.success == true){
							toastr.success('successfully deleted ' + manager.selected_file.name, "Sweet Success!");
							getFiles(options.baseDirectory);
							$('#confirm_delete_modal').modal('hide');
						} else {
							toastr.error(data.error, "Whoops!");
						}
					});
				});

				// Crop Image
				$('#crop').click(function(){
					// Cleanup the previous cropper
					if (typeof cropper !== 'undefined' && cropper instanceof Cropper) {
						cropper.destroy();
					}
					$('#confirm_crop_modal').modal('show');
				});

				// Cropper must init after the modal shown
				$('#confirm_crop_modal').on('shown.bs.modal', function (e) {
					var croppingImage = document.getElementById('cropping-image');
					console.log(croppingImage);
					cropper = new Cropper(croppingImage, {
						crop: function(e) {
							document.getElementById('new-image-width').innerText = Math.round(e.detail.width) + 'px';
							document.getElementById('new-image-height').innerText = Math.round(e.detail.height) + 'px';
							croppedData = {
								x: Math.round(e.detail.x),
								y: Math.round(e.detail.y),
								height: Math.round(e.detail.height),
								width: Math.round(e.detail.width)
							};
						}
					});
				});

				$('#crop_btn').click(function(){
					if (window.confirm($(this).data('confirm'))) {
						cropImage(false);
					}
				});

				$('#crop_and_create_btn').click(function(){
					cropImage(true);
				});

				/********** END TOOLBAR BUTTONS **********/

				manager.$watch('files', function (newVal, oldVal) {

					$('#filemanager #content #files').hide();
					$('#filemanager #content #files').fadeIn('fast');
					$('#filemanager .loader').fadeOut(function(){

						$('#filemanager #content').fadeIn();
					});
					if(newVal.items.length < 1){
						$('#no_files').show();
					} else {
						$('#no_files').hide();
                        var el = document.getElementById('files');
                        var sortable = Sortable.create(el);
					}
				});


				manager.$watch('selected_file', function (newVal, oldVal) {
					if(typeof(newVal) == 'undefined'){
						$('.right_details').hide();
						$('.right_none_selected').show();
						$('#move').attr('disabled', true);
						$('#delete').attr('disabled', true);
					} else {
						$('.right_details').show();
						$('.right_none_selected').hide();
						$('#move').removeAttr("disabled");
						$('#delete').removeAttr("disabled");
					}
				});

				function getFiles(folders){
					if( (folders != '/') && (typeof folders === 'object')) {
						var folder_location = '/' + folders.join('/');
					} else if (folders != '/') {
                        folder_location = folders;
					} else {
						var folder_location = '/';
					}
					$('#file_loader').fadeIn();
					$.post(options.baseUrl+'/media/files', { id: options.id, model: options.model, folder:folder_location, _token: CSRF_TOKEN, _token: CSRF_TOKEN }, function(data) {
						$('#file_loader').hide();
						manager.files = data;
						files.trigger('click');
                        manager.$forceUpdate();
                    });
				}

				function setCurrentSelected(cur){
					$('#files li .selected').removeClass('selected');
					$(cur).addClass('selected');
					manager.selected_file = manager.files.items[$(cur).data('index')];
				}

				function bytesToSize(bytes) {
					var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
					if (bytes == 0) return '0 Bytes';
					var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
					return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
				}

				function cropImage(createMode) {
					croppedData.originImageName = manager.selected_file.name
					croppedData.upload_path = options.baseDirectory
					croppedData.createMode = createMode

					var postData = Object.assign(croppedData, {_token: CSRF_TOKEN})
					$.post(options.baseUrl+'/media/crop', postData, function(data){
						console.log(data)
						if(data.success == true){
							toastr.success(data.message)
							getFiles(options.baseDirectory);
							$('#confirm_crop_modal').modal('hide');
						} else {
							toastr.error(data.error, "Whoops!");
						}
					});
				}


			}
		};

		var media = new VoyagerMedia({
		    baseUrl: document.getElementById('base_url').value,
            baseDirectory: document.getElementById('base_directory').value,
			id: document.getElementById('model_id').value,
			model: document.getElementById('model_class').value,
		});
		$(function () {
		    media.init();
		});

	}
}