<?php

namespace TCG\Voyager\Actions;

class ViewAction extends AbstractAction
{
    public function getTitle()
    {
        return __('voyager::generic.view');
    }

    public function getIcon()
    {
        return 'voyager-eye';
    }

    public function getPolicy()
    {
        return 'read';
    }

    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-warning pull-right view',
            'target' => '_new'
        ];
    }

    public function getDefaultRoute()
    {
        return  config('app.url'). DIRECTORY_SEPARATOR . $this->data->slug;
    }
}
