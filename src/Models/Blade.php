<?php

namespace TCG\Voyager\Models;

use Illuminate\Database\Eloquent\Model;


class Blade extends Model
{
    public function slugs()
    {
        return $this->hasMany('TCG\Voyager\Models\Slug');
    }
}
