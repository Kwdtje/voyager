<?php

namespace TCG\Voyager\Models;

use Illuminate\Database\Eloquent\Model;


class Header extends Model
{
    public $fillable = ['title', 'subtitle', 'content', 'link', 'link_text', 'page_id'];

    public function page()
    {
        return $this->belongsTo('TCG\Voyager\Models\Page');
    }

    public function images()
    {
        return $this->morphMany('TCG\Voyager\Models\Image', 'imageable');
    }
}
