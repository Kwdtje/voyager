<?php

namespace TCG\Voyager\Models;

use Illuminate\Database\Eloquent\Model;


class Slug extends Model
{
    protected $guarded = ['id'];

    public function __toString()
    {
        return empty($this->slug) ? "" : $this->slug;
    }

    public function slugable() {

        $this->morphTo();
    }

    public function blade() {

       return $this->belongsTo('\TCG\Voyager\Models\Blade');
    }
}
