<?php

namespace TCG\Voyager\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use TCG\Voyager\Traits\HasRelationships;
use TCG\Voyager\Traits\Translatable;

class Page extends Model
{
    use Translatable,
        HasRelationships;

    protected $translatable = ['title', 'body'];

    public $fillable = ['title', 'body', 'excerpt', 'meta_description', 'meta_keywords', 'seo_title'];

    /**
     * Statuses.
     */
    const STATUS_ACTIVE = 'ACTIVE';
    const STATUS_INACTIVE = 'INACTIVE';

    /**
     * List of statuses.
     *
     * @var array
     */
    public static $statuses = [self::STATUS_ACTIVE, self::STATUS_INACTIVE];

    protected $guarded = [];


    /**
     * Scope a query to only include active pages.
     *
     * @param  $query  \Illuminate\Database\Eloquent\Builder
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('status', static::STATUS_ACTIVE);
    }

    public function images()
    {
        return $this->morphMany('TCG\Voyager\Models\Image', 'imageable');
    }

    public function slug()
    {
        return $this->morphOne('TCG\Voyager\Models\Slug', 'slugable');
    }

}
