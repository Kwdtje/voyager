<?php

namespace TCG\Voyager\Models;

use Illuminate\Database\Eloquent\Model;


class Tag extends Model
{
    protected $guarded = ['id'];

    public function __toString()
    {
        return empty($this->slug) ? "" : $this->slug;
    }

    /**
     * Get all of the posts that are assigned this tag.
     */
    public function posts()
    {
        return $this->morphedByMany('Post', 'taggable');
    }

}
