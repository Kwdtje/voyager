<?php

namespace TCG\Voyager\Models;

use Illuminate\Database\Eloquent\Model;


class Image extends Model
{
    public $fillable = ['url', 'imageable_id', 'imageable_type', 'position'];

    public function imageable() {

        $this->morphTo();
    }
}
