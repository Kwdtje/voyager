<?php

namespace TCG\Voyager\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use League\Flysystem\Plugin\ListWith;
use TCG\Voyager\Facades\Voyager;

class VoyagerMediaController extends Controller
{
    /** @var string */
    private $filesystem;

    /** @var string */
    private $directory = '';

    public function __construct()
    {
        $this->filesystem = config('voyager.storage.disk');
    }

    public function index()
    {
        // Check permission
        Voyager::canOrFail('browse_media');

        return Voyager::view('voyager::media.index');
    }

    public function files(Request $request)
    {
        $folder = $request->folder;

        if ($folder == '/') {
            $folder = '';
        }

        $dir = $this->directory . $folder;

        return response()->json([
            'name' => 'files',
            'type' => 'folder',
            'path' => $dir,
            'folder' => $folder,
            'items' => $this->getFiles($dir, $request->get('id'), $request->get('model')),
            'last_modified' => 'asdf',
        ]);
    }

    public function upload(Request $request)
    {
        try {
            $realPath = Storage::disk($this->filesystem)->getDriver()->getAdapter()->getPathPrefix();

            $allowedImageMimeTypes = [
                'image/jpeg',
                'image/png',
                'image/gif',
                'image/bmp',
                'image/svg+xml',
            ];
            $file = $request->file->store($request->upload_path, $this->filesystem);

            if (in_array($request->file->getMimeType(), $allowedImageMimeTypes)) {
                $image = Image::make($realPath . $file);

                if ($request->file->getClientOriginalExtension() == 'gif') {
                    copy($request->file->getRealPath(), $realPath . $file);
                } else {
                    $image->orientate()->save($realPath . $file, 70);
                }
            }

            $success = true;
            $message = __('voyager::media.success_uploaded_file');
            $path = preg_replace('/^public\//', '', $file);
            $model = $request->get('model');
            if ($request->get('id')) {
                $id = $request->get('id');
            } else {
               $new = new $model(['title' => '[CONCEPT]']);
               $new->save();
               $id = $new->id;
            }
            $images = new \TCG\Voyager\Models\Image([
                'url' => '/storage' . DIRECTORY_SEPARATOR . $path,
                'imageable_id' => $id,
                'imageable_type' => $model,
                'position' => 0
            ]);
            $images->save();

        } catch (Exception $e) {
            $success = false;
            $message = $e->getMessage();
            $path = '';
        }

        return response()->json(compact('id','success', 'message', 'path'));
    }

    private function getFiles($dir, $id, $model)
    {
        $files = [];
        if ($model::find($id)) {
            $images = $model::find($id)->images;
            $storage = Storage::disk($this->filesystem)->addPlugin(new ListWith());
            $storageItems = $storage->listWith(['mimetype'], $dir);
            foreach ($storageItems as $item) {
                    if ($images->contains('url', Storage::url($item['path']))) {
                        $files[] = [
                            'name' => $item['basename'],
                            'type' => isset($item['mimetype']) ? $item['mimetype'] : 'file',
                            'url' => Storage::url($item['path']),
                            'path' => Storage::disk($this->filesystem)->url($item['path']),
                            'size' => $item['size'],
                            'last_modified' => $item['timestamp'],
                        ];
                    }
            }
        }
        return $files;
    }

    // REMOVE FILE
    public function remove(Request $request)
    {
        try {
            // GET THE SLUG, ex. 'posts', 'pages', etc.
            $slug = $request->get('slug');

            // GET image name
            $image = $request->get('image');

            // GET record id
            $id = $request->get('id');

            // GET field name
            $field = $request->get('field');

            // GET THE DataType based on the slug
            $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

            // Check permission
            Voyager::canOrFail('delete_' . $dataType->name);

            // Load model and find record
            $model = app($dataType->model_name);
            $data = $model::find([$id])->first();

            // Check if field exists
            if (!isset($data->{$field})) {
                throw new Exception(__('voyager::generic.field_does_not_exist'), 400);
            }

            // Check if valid json
            if (is_null(@json_decode($data->{$field}))) {
                throw new Exception(__('voyager::json.invalid'), 500);
            }

            // Decode field value
            $fieldData = @json_decode($data->{$field}, true);

            // Flip keys and values
            $fieldData = array_flip($fieldData);

            // Check if image exists in array
            if (!array_key_exists($image, $fieldData)) {
                throw new Exception(__('voyager::media.image_does_not_exist'), 400);
            }

            // Remove image from array
            unset($fieldData[$image]);

            // Generate json and update field
            $data->{$field} = json_encode(array_values(array_flip($fieldData)));
            $data->save();

            return response()->json([
                'data' => [
                    'status' => 200,
                    'message' => __('voyager::media.image_removed'),
                ],
            ]);
        } catch (Exception $e) {
            $code = 500;
            $message = __('voyager::generic.internal_error');

            if ($e->getCode()) {
                $code = $e->getCode();
            }

            if ($e->getMessage()) {
                $message = $e->getMessage();
            }

            return response()->json([
                'data' => [
                    'status' => $code,
                    'message' => $message,
                ],
            ], $code);
        }
    }

    // Crop Image
    public function crop(Request $request)
    {
        $createMode = $request->get('createMode') === 'true';
        $x = $request->get('x');
        $y = $request->get('y');
        $height = $request->get('height');
        $width = $request->get('width');

        $realPath = Storage::disk($this->filesystem)->getDriver()->getAdapter()->getPathPrefix();

        $originImagePath = $realPath . $request->upload_path . '/' . $request->originImageName;

        try {
            if ($createMode) {
                // create a new image with the cpopped data
                $fileNameParts = explode('.', $request->originImageName);
                array_splice($fileNameParts, count($fileNameParts) - 1, 0, 'cropped_' . time());
                $newImageName = implode('.', $fileNameParts);
                $destImagePath = $realPath . $request->upload_path . '/' . $newImageName;
            } else {
                // override the original image
                $destImagePath = $originImagePath;
            }

            Image::make($originImagePath)->crop($width, $height, $x, $y)->save($destImagePath);

            $success = true;
            $message = __('voyager::media.success_crop_image');
        } catch (Exception $e) {
            $success = false;
            $message = $e->getMessage();
        }

        return response()->json(compact('success', 'message'));
    }
}
